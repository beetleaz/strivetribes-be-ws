import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class EventsGateway {
  @WebSocketServer()
  server: Server;

  @SubscribeMessage('login')
  handleLogin(socket: Socket, data: string): string {
    if (!global.users) {
      global.users = {};
    }
    for (const [key, value] of Object.entries(global.users)) {
      if (value && data && value.toString() === data.toString()) {
        delete global.users[key];
      }
    }
    global.users[socket.id] = data;
    socket.broadcast.emit('user-add-msg', [
      ...new Set(Object.values(global.users)),
    ]);
    return data;
  }

  @SubscribeMessage('disconnect')
  handleDisconnect(socket: Socket): void {
    if (global.users && global.users[socket.id]) {
      socket.broadcast.emit('user-delete-msg', global.users[socket.id]);
      delete global.users[socket.id];
    }
  }
}
